# 参与贡献<a name="ZH-CN_TOPIC_0000001053868136"></a>

## 行为准则

UBML是一个开源社区。它完全依赖于社区提供友好的开发和协作环境，所以在参与社区贡献之前，请先阅读并遵守UBML社区的[行为守则](../CODE-OF-CONDUCT.md)。

## 开始贡献<a name="section184321756134618"></a>

### 联系组织

我们采用SIG（Special Interest Group）机制组织社区贡献，如果你对我们的开源项目感兴趣，可以找到对应的SIG组织与SIG Leader联系获得如何参与贡献指导。
我们现在成立了以下SIG组织，如果你对贡献某些领域感兴趣，也可以通过在邮件列表中发起评审，成立新的SIG组织。

| SIG组织名称 | SIG Leader   |
| :----: | :----: | 
| 云应用框架 SIG | [@Simon Ren](https://gitee.com/simonice) | 
| UBML SDK SIG | [@李斌](https://gitee.com/mopowoxo) | 
| 前端SIG | [@Sagi](https://gitee.com/chenshj) | 
| 领域服务SIG | [@宫保金](https://gitee.com/benjamin-gong) | 
| 数据库对象 SIG | [@周立杰](https://gitee.com/lijiezhou) | 

### 开源路线图

你可以参考各SIG组织制定的发展路线图挑选贡献领域，发起或者领取Issue参与贡献。

- [前端SIG - Roadmap](../sig/farris/roadmap.md)

### 代码风格

请遵循UBML编程规范，进行代码开发、检视、测试，务必保持代码风格统一。

-   [Java语言编程规范](java-coding-style-guide.md)

### 开源软件引入

若要引入新的第三方开源软件到UBML项目中，请参考[第三方开源软件引入指导](3rd-software-import-guide.md)

### 贡献工作流<a name="section15769105812369"></a>

有关详细信息，请参考[贡献流程](gitee-workflow.md)。

## 社区沟通与交流<a name="section98614457153"></a>

有关详细信息，请参考[社区沟通与交流](communication-guide.md)。