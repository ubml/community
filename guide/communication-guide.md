# 社区沟通与交流<a name="ZH-CN_TOPIC_0000001051853133"></a>

如果您在使用UBML过程中遇到问题，请加入邮件群组参与讨论，这是参与UBML项目讨论的正确方式。

## 如何订阅邮件列表<a name="section103251821112117"></a>

如果您以前从未订阅过邮件列表，请参照下面的操作步骤。

1.  点击您想要订阅的邮件列表的名称。
2.  浏览器将跳转到该邮件列表的订阅页面，那里将提供有关如何订阅的说明。
3.  阅读订阅说明，您需要提供一个您希望用来订阅邮件列表的电子邮件地址。
4.  输入您的电子邮件地址并点击订阅，您将收到一封电子邮件，要求您确认订阅。
5.  回复您收到的电子邮件以确认您的订阅。
6.  最后您将收到来自一封来自邮件列表的欢迎邮件。

**表 1**  : 邮件列表

|邮件列表地址| 简介|功能描述|
|:-----------|:-------------------- |:-------------------------- |
|ubml-user@openatom.io|用户邮件列表| 面向UBML社区用户公的邮件列表，用于开讨论项目使用问题、寻求帮助、经验交流等任何开源相关话题，任何用户可</span><a href="https://lists.openatom.io/postorius/lists/ubml-user.openatom.io/" target="_blank" rel="noopener noreferrer">订阅</a><span>|
|ubml-dev@openatom.io| 开发者邮件列表|面向UBML社区开发者（Contributer/Committer）的邮件列表，可公开讨论项目问题、特性与版本规划、技术经验交流等任何开源相关话题，任何开发者可</span><a href="https://lists.openatom.io/postorius/lists/ubml-dev.openatom.io/" target="_blank" rel="noopener noreferrer">订阅</a><span>  |


## 如何发送邮件到邮件列表<a name="section09801118222"></a>

要将邮件发送到指定的邮件列表，请向上表中列出的邮件地址发送您的电子邮件。

这样所有在这个邮件列表中的社区成员都能收到您的电子邮件。

