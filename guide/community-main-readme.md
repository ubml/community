# 项目介绍
UBML（Unified-Business-Modeling-Language）是开放原子开源基金会（OpenAtom Foundation）孵化及运营的项目，它是一种基于领域特定语言的、用于快速构建应用软件的低代码开发建模语言，来源于浪潮iGIX团队，是浪潮iGIX企业数字化能力平台的核心低代码体系。

![alt UBML结构示意](../figures/ubml-readme-figure.png)

UBML作为低代码开发平台的开发语言，是低代码开发平台的核心基础，包含开发语言无关性的建模标准（UBML-Standard），开发微服务应用的基础框架（UBML-Framework），内置了基于UBML标准的全栈业务模型（UBML-Models），并提供了可与模型进行全生命周期交互的开发服务与套件，（UBML-SDK）以及支撑模型运行的运行时框架（UBML-Runtime）。未来UBML还将引入更多低代码开发工具（UBML-Designer等），形成完整的低代码开发平台。

# 仓库介绍

UBML采取微服务架构模式，遵循领域驱动设计分层架构，最底层caf层为开发微服务的基础框架，模型从下往上依次为repository（持久化层）、component（构件层）、domain（领域层）、bff（服务于前端的后端）、restapi（rest服务层）、ui（界面展现层）。
提供了metadata（元数据框架）用于提供统一的元数据管理,sdk（软件开发工具包）辅助UBML模型开发。具体结构如下所示：

![alt UBML结构示意](../figures/ubml-readme-repositories.png)

### caf（云开发框架）
UBML开发微服务应用的基础框架，提供了各种基础组件，如Cache、RPC、Log等，主要有 caf-framework 和 caf-boot 等仓库：
caf-framework：UBML开发微服务应用的基础框架，提供了各种Cache，RPC，Log等等基础组件；
caf-boot：针对caf-framework基础框架各个组件的自动装配，简化了开发依赖，使得framework基础组件可以以插件方式在运行时灵活的配置加载。

### repository （持久化层）
UBML持久化模型层，提供数据库对象模型结构描述（关系型数据库的表、视图等）以及数据库对象的维护（创建、更新结构等），主要有database-object-model和database-object-service等仓库：
database-object-model：
database-object-service：

### component (构件层)
UBML构件模型层，提供UBML构件基础结构和服务，主要有 component-model 和component-framework等仓库：
component-model：
component-framework：
### domain （领域层）
UBML领域模型层，提供UBML领域模型（业务实体模型）结构描述、运行框架、解析引擎以及代码生成服务，主要有 business-entity-model、business-entity-framework、business-entity-generator、business-entity-engine等仓库：
business-entity-model：
business-entity-framework：
business-entity-generator：
business-entity-engine：

### bff(服务于前端的后端)
bff（backend for frontend），提供UBML应用服务模型（视图对象模型）结构描述、运行框架、解析引擎以及代码生成服务，主要有 view-object-model、bff-framework、bff-generator、bff-engine等仓库：
view-object-model：
bff-framework：
bff-generator：
bff-engine：
### restapi（rest服务层）
UBML rest服务模型层，提供UBML rest服务模型结构描述、服务发布等内容，主要有 rest-api-model、rest-api-engine、rest-api-generator等仓库：
rest-api-model：
rest-api-engine：
rest-api-generator：

### ui（用户交互层）
UBML 用户交互模型层，提供适配三个技术框架（angular、vue、react）的ui组件、主题定制工具、开发框架和页面解析生成引擎，主要有  等仓库：

### metadata（元数据框架）

### sdk（软件开发工具包）


# 如何参与
参与社区，请参见 [社区介绍](../README.md) 
参与贡献，请参见 [参与贡献](contribute-guide.md)

# 联系方式

邮箱：UBML@inspur.com

邮件列表：请参见 [社区沟通与交流](communication-guide.md)