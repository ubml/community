# UBML社区

欢迎来到UBML社区。

## 介绍

代码仓Community保存了关于UBML社区的所有信息，包括社区治理、社区活动、开发者贡献指南、沟通交流指南等内容。 

## 社区治理组织架构
UBML社区通过项目管理委员会（Project Management Committee）进行管理。
- [项目管理委员会](./zh-cn/pmc.md)
- [Committer](./zh-cn/committer.md)

## 如何在本代码仓贡献
请阅读[如何贡献](/CONTRIBUTING.md)获得帮助。

## 签署CLA协议
您必须首先签署CLA(Contributor License Agreement)协议，然后才能参与社区贡献。

## 社区交流
UBML社区通过邮件列表进行交流
| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| ubml-user@openatom.io | 用户邮件列表 | UBML社区用户讨论邮件列表 [订阅](https://lists.openatom.io/postorius/lists/ubml-user.openatom.io/)|
| ubml-dev@openatom.io | 开发邮件列表 | UBML社区开发讨论邮件列表 [订阅](https://lists.openatom.io/postorius/lists/ubml-dev.openatom.io/)|