# UBML代码仓库


本文简要描述了UBML社区中包含的代码仓及其介绍：

|名称| 内容描述 |仓库类型|
|:---|:-------------------- |:--------------- |
|ubml-standard|UBML低代码建模体系标准，包括DSL标准描述以及标准的规范化文档 |public|
|ubml-impl|基于UBML-standard的Java语言默认实现，包括模型定义（Models）、建模开发服务与套件（SDK）、模型运行框架（Runtime）等模块| public |
|ubml-community|关于UBML社区的所有信息，包括社区治理、社区活动、开发者贡献指南、沟通交流指南等内容| public |