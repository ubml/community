## UBML技术委员会（TC）

### UBML技术委员会（TC）是UBML项目管理委员会（PMC）下设唯一的技术委员会。
### UBML技术委员会是本项目的技术领导机构。技术委员会在合法合规并严格遵循基金会相关制度规定的前提下，依照项目开源治理制度开展工作。
### 技术委员会委员对UBML开源社区技术方向负责，受UBML开源社区全体成员监督。

## UBML TC成员列表
| 姓名 | 账号   | 角色 |
| :----: | :----: | :----: |
| 郑伟波 | [@izhengwb](https://gitee.com/izhengwb) | 委员 |
| 孙立新 | [@slx_7](https://gitee.com/slx_7) | 委员 |
| 陈圣杰 | [@chenshj](https://gitee.com/chenshj) | 委员 |
| 仪思奇 | [@yisiqi](https://gitee.com/yisiqi) | 委员 |
| 彭晓迪 | [@little-fool-lf](https://gitee.com/little-fool-lf) | 委员 |
| 周立杰 | [@lijiezhou](https://gitee.com/lijiezhou) | 委员 | 
| 宫保金 | [@benjamin-gong](https://gitee.com/benjamin-gong) | 委员 |