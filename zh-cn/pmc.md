## UBML项目管理委员会（PMC）

   UBML项目管理委员会（PMC: Project Management Committee）负责UBML社区管理，职责如下：
1. 负责社区管理工作，包括开源社区版本规划、架构看护、特性代码开发维护、版本及补丁规划等；
2. 发布和处理社区需求，为开源社区提供技术架构指导和技术决策；
3. 组织社区安全工作，及时进行安全漏洞扫描、响应、处理等工作；
4. 处理社区Bug、issue、邮件列表，闭环周期满足开源社区的SLA要求；
5. 负责PMC、Committer成员的[选举和退出](./guidelines_role_grouth.md)，制定PMC、Committer协作机制；


## UBML PMC成员列表
| 姓名 | 账号   | 角色 | 领域 |
| :----: | :----: | :----: | :----: |
| 郑伟波 | [@izhengwb](https://gitee.com/izhengwb) | PMC主席 | 总架构 |
| 仪思奇 |[@yisiqi](https://gitee.com/yisiqi) | PMC成员 | 发布 |
| 魏子重 | [@ethanw](https://gitee.com/ethanw) | PMC成员 | 运营 |
| 彭晓迪 | [@little-fool-lf](https://gitee.com/little-fool-lf) | PMC成员 | 安全 |
| 陈圣杰 | [@chenshj](https://gitee.com/chenshj) | PMC成员 | 前端SIG |
| 宫保金 | [@benjamin-gong](https://gitee.com/benjamin-gong) | PMC成员 | 领域服务SIG |


## PMC会议链接
- 会议时间: 每双周周五 10:30-12:00
- 议题申报: UBML PMC Meeting Proposal
- 会议主题: 通过邮件通知
- 会议通知: 请[订阅](https://lists.openatom.io/postorius/lists/ubml-dev.openatom.io/)邮件列表 ubml-dev@openatom.io 获取会议链接

## 联系方式

| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| ubml-user@openatom.io | 用户邮件列表 | UBML社区用户讨论邮件列表 [订阅](https://lists.openatom.io/postorius/lists/ubml-user.openatom.io/)|
| ubml-dev@openatom.io | 开发邮件列表 | UBML社区开发讨论邮件列表 [订阅](https://lists.openatom.io/postorius/lists/ubml-dev.openatom.io/)|

