# 项目介绍
UBML（Unified-Business-Modeling-Language）是开放原子开源基金会（OpenAtom Foundation）孵化及运营的项目，它是一种用于快速构建应用软件的低代码领域建模语言，来源于浪潮海岳iGIX团队，是浪潮海岳PaaS平台iGIX的核心低代码建模体系。

![alt UBML结构示意](/figures/ubml-readme-figure.png)

UBML作为低代码开发平台的开发语言，是低代码开发平台的核心基础，包含开发语言无关性的建模标准（UBML-Standard），开发微服务应用的基础框架（CAF-Framework），内置了基于UBML标准的全栈业务模型（UBML-Models），并提供了可与模型进行全生命周期交互的开发服务与套件（UBML-SDK）以及支撑模型运行的运行时框架（UBML-Runtime）。

# 技术特征
## 开放性
UBML提供了一套独立于模型实现的标准UBML-Standard，UBML所面向的应用类型、模型种类和模型数量是可以进行扩展的。
## 开发语言无关性
UBML的模型具有开发语言无关性，UBML模型是基于领域特定语言DSL描述，比如JSON、XML等，可转化成多种开发语言的实现，比如Java、Python、C#等（目前提供了Java实现）。
## 云原生
UBML 遵循云原生理念设计，基于微服务架构，实现支持容器化部署。
## 模型工程化
UBML具有工程化结构，支持与源代码管理、制品管理库、CI/CD等工程化工具，无缝融合入DevOps等现代化研发流程。
## 全栈模型刻画
UBML从应用分层架构出发，结合了微服务架构、领域驱动设计理念，把企业业务抽象模型化，用元数据的方式描述业务的模型，形成了覆盖持久化层、领域层、业务流程层、BFF 层、UI 层的全栈模型体系，为业务应用开发提供全栈的建模开发支撑。
## 运行态定制
采用代码生成 + 动态解析，支持Hybrid模式，运行态仍可进行个性化定制。

## 
# 仓库介绍

UBML仓库结构，请点击[这里](ubml-repositories.md)查看。

# 快速入门
[请阅读快速开始](https://ibc.inspures.com/inbuilder/community/docs/#/doc/md/main)
<!---
# 开发者文档

# 示例教程
--->

# 如何参与
## 从使用低代码平台发行版开始
UBML是一套低代码建模体系，是低代码平台的内核，包括了标准、运行时和SDK，通过内核+发行版的模式为不同场景的低代码平台封装提供可能  
低代码平台发行版是在UBML低代码内核基础上进行封装后的一体化低代码平台，具备可视化建模开发和运行的完整功能  
成为贡献者，先成为使用者，参与到UBML社区，建议先从使用低代码发行版入手

| 低代码发行版                                 | 提供者        | 获取地址                   |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| inBuilder社区版 | 浪潮 | [前往下载](https://ibc.inspures.com/) |


## 参与社区，请参见 [社区介绍](../README.md) 

## 参与贡献，请参见 [参与贡献](../guide/contribute-guide.md)

# 许可协议
UBML主要遵循Apache License V2.0协议，详情请参考各代码仓LICENSE声明。
UBML引用第三方开源软件及许可证说明，参考[第三方开源软件说明](../guide/3rd-software-import-guide.md)。

# 邮件列表
| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| ubml-user@openatom.io | 用户邮件列表 | UBML社区用户讨论邮件列表 [订阅](https://lists.openatom.io/postorius/lists/ubml-user.openatom.io/)|
| ubml-dev@openatom.io | 开发邮件列表 | UBML社区开发讨论邮件列表 [订阅](https://lists.openatom.io/postorius/lists/ubml-dev.openatom.io/)|
