## 仓库总体结构


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBML从应用分层架构出发，结合了微服务架构、领域驱动设计理念，把企业业务抽象模型化，用元数据的方式描述业务的模型，形成了覆盖持久化层、领域层、业务流程层、BFF 层、UI 层的全栈模型体系，为业务应用开发提供全栈的建模开发支撑。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;基于UBML开发时，可以通过可视化设计器建立模型，模型都是基于领域特定语言DSL描述，比如JSON、XML等，覆盖了名称、Code、类型等信息，可以通过该描述，依据不同实际需求，可转化成多种开发语言的实现，比如JAVA、Python、C#等等，UBML通过一份模型屏蔽了多种开发语言的差异。目前UBML项目内置了后端Java语言、前端Angular语言的实现，欢迎贡献其他语言的实现。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBML支持代码生成、解析两种运行模式，提供了面向特定语言的代码生成器（Generators）和直接运行的解析器（Engines），在运行时支持生成+解析混合运行模式，可以运行时定制。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBML仓库整体结构如下图：
![alt UBML仓库结构](../figures/ubml-readme-repositories.png)
### caf（云开发框架）
UBML开发微服务应用的基础框架，提供了各种基础组件，如Cache、RPC、Log等，主要有 caf-framework 和 caf-boot 等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;caf-framework:UBML开发微服务应用的基础框架，提供了各种Cache，RPC，Log等等基础组件；

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;caf-boot:针对caf-framework基础框架各个组件的自动装配，简化了开发依赖，使得framework基础组件可以以插件方式在运行时灵活的配置加载。

### repository （持久化层）
UBML持久化模型层，提供数据库对象模型结构描述（关系型数据库的表、视图等）以及数据库对象的维护（创建、更新结构等），主要有database-object-model和database-object-service等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;database-object-model:数据库对象模型仓库，包含实体类及其属性，提供get、set方法和构造函数。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;database-object-service:数据库对象业务逻辑仓库，包含对外提供的数据库对象创建、获取、部署等服务。

### component (构件层)
UBML构件模型层，提供UBML构件基础结构和服务，主要有 component-model 和component-framework等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;component-model:UBML构件元数据模型结构仓库，描述了构件模型的主要结构。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;component-framework:UBML 构件模型运行时框架，包含了构件运行时相关服务。
### domain （领域层）
UBML领域模型层，提供UBML领域模型（业务实体模型）结构描述、运行框架、解析引擎以及代码生成服务，主要有 business-entity-model、business-entity-framework、business-entity-generator、business-entity-engine等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;business-entity-model:业务实体模型仓库，包含业务实体模型的结构和业务实体模型的持久化服务。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;business-entity-framework:业务实体框架仓库，业务实体的运行时框架，基于业务实体生成的代码或者业务实体解析引擎都是在业务实体框架的基础上运行的。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;business-entity-generator:业务实体生成器仓库，将业务实体模型生成为可运行的Java代码。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;business-entity-engine:业务实体解析引擎仓库，读取业务实体进行解析运行。

### bff(服务于前端的后端)
bff（backend for frontend），提供UBML应用服务模型（视图对象模型）结构描述、运行框架、解析引擎以及代码生成服务，主要有 view-object-model、bff-framework、bff-generator、bff-engine等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;view-object-model:视图对象模型仓库，包含视图对象模型的结构和视图对象模型的持久化服务。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bff-framework:BFF运行时框架仓库，BFF的运行时框架，基于视图对象模型生成的代码或者BFF解析引擎都是在BFF框架的基础上运行的。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bff-generator:BFF生成器仓库，将视图对象模型生成为可运行的Java代码。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bff-engine:BFF解析引擎，读取视图对象模型进行解析运行。
### restapi（rest服务层）
UBML rest服务模型层，提供UBML rest服务模型结构描述、服务发布等内容，主要有 rest-api-model、rest-api-engine、rest-api-generator等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rest-api-model:UBML Rest服务层模型--Rest服务模型，描述了Rest服务的信息和服务方法集合。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rest-api-engine:UBML Rest服务层模型的解析引擎，可将Rest服务模型解析运行。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;rest-api-generator:UBML Rest服务层生成器，可将Rest服务模型生成为可运行的代码。

### ui（用户交互层）
UBML 用户交互模型层，提供适配三个技术框架（angular、vue、react）的ui组件、主题定制工具、开发框架和页面解析生成引擎，主要有ui-core、ui-angular、ui-react、ui-vue、theme-builder、devkit、jit-engine  等仓库:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ui-core:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ui-angular:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ui-react:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ui-vue:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;theme-builder:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;devkit:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;jit-engine:
### metadata（元数据框架）
UBML模型生命周期管理框架，提供UBML模型公共结构、运行时服务、以及设计时服务，包括模型的加载、查询、保存等。主要有metadata-common、metadata-service、meadata-service-dev 等仓库：

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;metadata-common:UBML元数据模型公共结构，里面包含元数据基本信息、元数据引用关系、元数据内容抽象类等内容。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;metadata-service:元数据服务，支持运行时元数据加载、外部推送元数据保存、运行时元数据扩展等服务。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;metadata-service-dev:元数据设计时服务，包括设计时元数据工程、元数据相关的生命周期管理服务。
### sdk（软件开发工具包）
UBML模型开发工具框架，主要包含模型的设计时服务，包括对模型进行读/写、编译、打包、提取、部署等通用的开发时API。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ubml-sdk:模型开发工具框架，提供模型设计时服务调用入口。