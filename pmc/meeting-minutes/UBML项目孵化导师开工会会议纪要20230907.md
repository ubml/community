# UBML项目开工会会议纪要
***日期**：2023-09-07*  
***时间**：11:00-12:00(UTC/GMT+8:00)*  
***地点**：浪潮科技园S01楼101会议室*  
***会议主持**：陈圣杰*  
***与会人**：谭中意、马涛（线上）、熊伟、许勇、杨东杰（线上）、郑伟波、魏子重、彭晓迪、孙立新、陈圣杰、霍亮、周立杰、任雪冰、李颖、史继新*  
**会议纪要：**  
UBML项目于2023年9月7日在浪潮科技园S01楼101会议室，召开项目开工会。
首先，由UBML项目负责人郑伟波为UBML项目导师颁发“UBML社区TOC导师聘书”。
接下来，陈圣杰从项目运营情况、项目组织和项目规划三个方面对UBML项目进行陈述。开放原子开源基金会TOC主席谭中意，UBML项目导师熊伟、许勇、马涛，对项目运营与发展提出建议与指导：

1.UBML项目已经有成熟的产品基础，接下来项目运营重点应该是项目上下游生态。  
2.邀请项目上下游合作伙伴、用户代表等IT部分负责人加入项目管理委员会和用户委员会，建立项目合作生态，发展商业合作。  
3.如果能促进围绕开源项目开展商业合作，可获得更积极并且更高质量的贡献。  
4.可以参考“四人法则”确定开源仓库项目粒度。调研主流开源项目仓库，主要贡献者一般维持4～5人，过于庞大的仓库不利于项目研发效率。  
5.创建SIG组织时，不一定按照功能模块划分SIG，避免在项目初期建立过于碎片化的SIG，可以按照共同完成一件事项建立SIG，需要建立SIG准入和退出机制。  
6.优先发展项目使用生态，以使用项目带动项目贡献。  
7.不局限贡献者的身份，上下游的合作伙伴都可以以各种形式参加到开源社区中  
8.贡献不局限于代码，也包括文档、案例、最佳实践。  
9.为项目贡献者提供仪式感，通过项目周边（吉祥物）、证书等形式，获得社区归属感。  
10.定期召开项目会议，固定项目会议时间，将其流程化，培养社区习惯。开会提前收集议题，要公开会议纪要，记录会议形成的待办事项，持续跟踪、反馈项目待办事项。





















